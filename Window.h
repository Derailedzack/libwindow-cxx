class Window{
public:
    virtual void CreateWindow(int width,int height) = 0;
    virtual void RenderLoop() = 0;
    virtual void EventLoop() = 0;
    virtual void OnWindowCreatationError();
}
